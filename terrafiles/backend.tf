terraform {
  backend "gcs" {
    bucket     = "playground-213512-terraform"
    prefix     = "tf-es-custom-machine/state"
  }
}
